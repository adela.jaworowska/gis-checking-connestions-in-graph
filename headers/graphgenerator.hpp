#ifndef GRAPHGENERATOR_HPP
#define GRAPHGENERATOR_HPP

#include "graph.hpp"
#include <exception>
#include <memory>

class GraphGenerator {

    void flushGeneratedEdges(Graph& graph, std::vector<std::pair<int,int>> edges, long long& currentNumberOfEdges, 
                             const long long& desiredNumberOfEdges) const;

    /**
    *    Metoda dodająca do grafu graf nieskierowany spójny o zadanej liczbie wierzchołków i krawędzi.
    */
    void addRandomConnectedGraph(Graph& graph, const int& numberOfVertices, const long long& numberOfEdges, const int& firstId) const;

    public:

    GraphGenerator();
    ~GraphGenerator();

    /**
    *    Metoda zwracająca graf nieskierowany (niekoniecznie spójny) o zadanej liczbie wierzchołków i składowych.
    *    W przypadku podania niewłaściwych argumentów rzuca wyjątek typu "GraphGeneratorException".
    */
    Graph generateGraph(const int& numberOfVertices, const int& numberOfComponents) const;

    /**
    *    Metoda zwracająca graf nieskierowany spójny o zadanej liczbie wierzchołków i krawędzi.
    *    W przypadku podania niewłaściwych argumentów rzuca wyjątek typu "GraphGeneratorException".
    */
    Graph generateConnectedGraph(const int& numberOfVertices, const long long& numberOfEdges) const;

};

class GraphGeneratorException : public std::exception 
{
    const std::string info;

    public:
    GraphGeneratorException(const std::string& msg);
    const char* what() const throw();
};

#endif