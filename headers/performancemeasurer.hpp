#ifndef PERFORMANCEMEASURER_HPP
#define PERFORMANCEMEASURER_HPP

#include "graphgenerator.hpp"
#include <chrono>
#include <fstream>
#include <random>
#include <algorithm>

class PerformanceMeasurer {
    using TimePoint = std::chrono::steady_clock::time_point;

    class Statistics {
        std::vector<double> values;
        double mean=0, standardDeviation=0, min=std::numeric_limits<double>::infinity(), max=0;
        double quartile1, quartile2, quartile3;

        void calculateQuantilesEven();
        void calculateQuantilesOdd();

        public:

        void loadValue(const double& value);
        void calculateStdDev();
        void calculateQuantiles();
        friend std::ostream& operator<<(std::ostream& os, const Statistics& stats)
        {
            os << stats.mean << " " << stats.standardDeviation << " " << stats.min << " " << stats.max 
               << " " << stats.quartile1 << " " << stats.quartile2 << " " << stats.quartile3 << " ";
            return os;
        }
    };

    GraphGenerator generator;

    TimePoint now() const;

    double elapsedTime(const TimePoint& begin, const TimePoint& end) const;

    std::pair<int, int> generateCheckedVertices(const int& n) const;

    void measureQueriesPerGraph(const Graph& graph, Statistics& statistics, const int& queriesPerGraph) const;

    /**
    *   Metoda wykonująca pomiary czasu działania metody "areVerticesConnected" z klasy "Graph".
    *   Parametry to: liczba wierzchołków, liczba grafów o danej liczbie wierzchołków, dla których mierzony
    *   jest czas i liczba wykonań metody dla każdego badanego grafu.
    *   Dla każdej liczby wierzchołków wykonywana jest liczba pomiarów równa trialGraphs*queriesPerGraph. Z wyników liczona jest średnia
    *   oraz odchylenie standardowe.
    *   Zwraca instancję klasy zawierającej informacje o średniej z wykonanych pomiarów, odchyleniu standardowym, minimalnej wartości,
    *   maksymalnej wartości oraz o kwartylach.
    */
    Statistics measurePerformanceForN(const int& n, const int& trialGraphs, const int& queriesPerGraph) const;

    public:

    PerformanceMeasurer();
    ~PerformanceMeasurer();

    /**
    *   Metoda wykonująca pomiary czasu działania metody "areVerticesConnected" z klasy "Graph".
    *   Parametry to: ścieżka do folderu, nazwa pliku, początkowa liczba wierzchołków, zmiana liczby wierzchołków co serię pomiarów,
    *   liczba grafów o danej liczbie wierzchołków, dla których mierzony jest czas i liczba wykonań metody dla każdego badanego grafu.
    *   Dla każdej liczby wierzchołków wykonywana jest liczba pomiarów równa trialGraphs*queriesPerGraph. Wyniki zawierają informacje o:
    *   średniej z wykonanych pomiarów, odchyleniu standardowym, minimalnej wartości, maksymalnej wartości oraz kwartylach.
    */
    void measureVertexPerformance(const std::string& resultDirPath, const std::string& fileName, const int& firstN,
                            const int& deltaN, const int& numberOfMeasurements, const int& trialGraphs, const int& queriesPerGraph) const;

    /**
    *   Metoda wykonująca pomiary czasu działania metody "areVerticesConnected" z klasy "Graph".
    *   Parametry to: ścieżka do folderu, nazwa pliku, liczba wierzchołków, minimalna liczba krawędzi,
    *   wartość, o którą zwiększa się liczba krawędzi przy każdym z testów, liczba badanych grafów (różnych liczb krawędzi), 
    *   dla których mierzony jest czas i liczba wykonań metody dla każdego badanego grafu.
    *   Dla każdego grafu wykonywana jest liczba pomiarów równa trialGraphs*queriesPerGraph. Wyniki dla każdej liczby wierzchołków są grupowane i
    *   zawierają informacje o: średniej z wykonanych pomiarów, odchyleniu standardowym, minimalnej wartości, maksymalnej wartości oraz kwartylach.
    */
    void measureEdgePerformance(const std::string& resultDirPath, const std::string& fileName, const int& n,
                                const long long& minEdges, const long long& deltaEdges, const int& trialGraphs, 
                                const int& queriesPerGraph) const;

};

class PerformanceException : public std::exception 
{
    const std::string info;

    public:
    PerformanceException(const std::string& msg);
    const char* what() const throw();
};

#endif