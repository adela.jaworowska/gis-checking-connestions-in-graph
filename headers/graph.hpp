#ifndef GRAPH_HPP
#define GRAPH_HPP

#include "vertex.hpp"
#include <unordered_map>
#include <boost/iterator/transform_iterator.hpp>

enum class Result {
    None, Close, Transitive
};

inline const char* toString(Result v)
{
    switch (v)
    {
        case Result::None:          return "None";
        case Result::Close:         return "Close";
        case Result::Transitive:    return "Transitive";
        default:                    return "[Unknown Result]";
    }
}

class Graph {

    struct SharedPtrConstTransform
    {
        template<class K, class T>
        std::shared_ptr<const T> operator()(const std::pair<K, std::shared_ptr<T>>& p) const
        {
            return p.second;
        }
    };

    long long numberOfEdges = 0;
    std::unordered_map<int, std::shared_ptr<Vertex>> vertices;

    /**
    *   Metoda ustawiająca wszystkie wierzchołki jako nieodwiedzone.
    */
    void setAllToUnvisited() const;

    public:

    Graph();
    Graph(const Graph& other);
    ~Graph();

    /**
    *    Metoda tworząca wierzchołek o zadanym id i dodająca go do grafu.
    *    Zwraca id wierzchołka, jeśli operacja udała się. W.p.p. -1.
    */
    int addVertex(const int& id);

    /**
    *    Metoda dodająca krawędź pomiędzy wierzchołkami vertex1, a vertex2.
    *    Zwraca true, jeśli operacja się udała. W.p.p. false.
    */
    bool addEdge(const int& vertex1, const int& vertex2);

    /**
    *    Metoda zwracająca liczbę krawędzi.
    */
    long long getNumberOfEdges() const;

    /**
    *    Metoda zwracająca wierzchołek o zadanym identyfikatorze.
    */
    const Vertex* getVertex(const int& id) const;

    using VertexIterator = boost::transform_iterator<SharedPtrConstTransform, std::unordered_map<int, std::shared_ptr<Vertex>>::const_iterator>;

    /**
    *   Metody zwracające iteratory pozwalające na iterację po wierzchołkach należących do grafu.
    */
    VertexIterator begin() const;
    VertexIterator end() const;

    /**
    *   Metoda zwracająca liczbę wierzchołków w grafie.
    */
    int getNumberOfVertices() const;

    /**
    *   Metoda sprawdzająca czy dwa wierzchołki o zadanych numerach są połączone blisko, przechodnio lub wcale.
    */
    Result areVerticesConnected(const int& vertex1, const int& vertex2) const;

};

#endif