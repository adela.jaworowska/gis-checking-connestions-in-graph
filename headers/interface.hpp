#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "graphgenerator.hpp"
#include "graphfileio.hpp"
#include "performancemeasurer.hpp"
#include <boost/filesystem.hpp>

#include <queue>

const std::unordered_map<std::string, std::string> font
{
    {"REDF", "\u001B[31m"},                       // Errors
    {"BGREENF", "\u001B[38;5;2m"},                // Regular output
    {"SKYF", "\u001B[38;5;39m"},
    {"RESETF", "\u001B[0m"}                       // Reset console font
};

class Interface{

    GraphGenerator generator;
    Graph graph;
    GraphFileIO fileIO;
    PerformanceMeasurer performanceMeasurer;
    boost::filesystem::path generatedGraphsPath;
    boost::filesystem::path dotFilesPath;
    boost::filesystem::path txtFilesPath;
    boost::filesystem::path pngFilesPath;
    boost::filesystem::path performanceTestsFilesPath;

    fd_set ready;
    struct timeval to;
    std::vector<char> buffer;
    std::queue<std::string> commandQueue;
    bool breakFlag = false;

    void createDirIfNonexistent(const boost::filesystem::path& path);
    void printHelp();
    void printGraph(const Graph& graph);
    void handleGenerate(const std::vector<std::string>& args);
    void handleGenerateConnected(const std::vector<std::string>& args);
    void handleLoadFromFile(const std::vector<std::string>& args);
    void handleSaveToFile(const std::vector<std::string>& args);
    void handleSaveToPng(const std::vector<std::string>& args);
    void handlePrintDirContents(const boost::filesystem::path& path);
    void handlePerformAlgorithm(const std::vector<std::string>& args);
    void handlePerformVertexMeasurements(const std::vector<std::string>& args);
    void handlePerformEdgeMeasurements(const std::vector<std::string>& args);

    void getUserCommands();
    void processCommands(const char* buf);
    std::vector<std::string> splitBySpace(const std::string &input);
    void handleCommands();

    public:

    Interface();
    ~Interface();
    void run();
};

class InterfaceException : public std::exception 
{
    const std::string info;

    public:
    InterfaceException(const std::string& msg);
    const char* what() const throw();
};

#endif