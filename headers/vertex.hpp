#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <vector>
#include <iterator>
#include <memory>
#include <algorithm>
#include <boost/iterator/transform_iterator.hpp>

class Vertex {
    friend class Graph;

    struct WeakPtrConstTransform
    {
        template<class T>
        std::weak_ptr<const T> operator()(const std::weak_ptr<T>& p) const
        {
            return p;
        }
    };

    struct IsSmaller
    {
	    bool operator()(const std::weak_ptr<Vertex>& a, const std::shared_ptr<Vertex>& b) const
        {
            return a.lock()->getId() < b->getId();
        }

        bool operator()(const std::shared_ptr<Vertex>& a, const std::weak_ptr<Vertex>& b) const
        {
            return a->getId() < b.lock()->getId();
        }
    };

    int id;
    bool visited;
    std::vector<std::weak_ptr<Vertex>> neighbours;

    /**
    *    Metoda zwracająca iterator na pozycji sąsiada o zadanym identyfikatorze.
    */
    const std::vector<std::weak_ptr<Vertex>>::const_iterator findNeighbourById(const int& id) const;

    public:

    Vertex(const int& id);
    ~Vertex();

    
    /**
    *    Metoda sprawdzająca czy wierzchołek o danym id jest sąsiadem.
    */
    const bool isNeighbour(const int& id) const;

    /**
    *    Metoda dodająca wierzchołek mający zostać sąsiadem.
    *    Zwraca id sąsiada, jeśli sąsiada wcześniej nie było w wektorze sąsiadów lub jego id nie jest równe id wierzchołka. W.p.p. -1.
    */
    int addNeighbour(const std::shared_ptr<Vertex>& neighbour);

    /**
    *    Metoda usuwająca sąsiada o zadanym identyfikatorze.
    *    Zwraca id usuniętego sąsiada, jeśli operacja się powiodła. W.p.p. -1.
    */
    int deleteNeighbour(const int& id);

    /**
    *    Zwraca identyfikator wierzchołka.
    */
    const int& getId() const;

    using NeighbourIterator = boost::transform_iterator<WeakPtrConstTransform, std::vector<std::weak_ptr<Vertex>>::const_iterator>;

    /**
    * Metody zwracające iteratory pozwalające na iterację po wierzchołkach sąsiadujących.
    */
    NeighbourIterator begin() const;
    NeighbourIterator end() const;
};

#endif