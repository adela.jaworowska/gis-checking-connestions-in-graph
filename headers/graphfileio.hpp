#ifndef GRAPHFILEIO_HPP
#define GRAPHFILEIO_HPP

#include "graph.hpp"
#include <graphviz/gvc.h>

class GraphFileIO {

    public:

    GraphFileIO();
    ~GraphFileIO();

    /**
    *    Metoda zapisująca graf do pliku tekstowego.
    *    Rzuca wyjątek w razie niepowodzenia.
    */
    void saveToTxt(const Graph& graph, const std::string& dirPath, const std::string& fileName) const;

    /**
    *    Metoda zapisująca graf do pliku o rozszerzeniu dot.
    *    Rzuca wyjątek w razie niepowodzenia.
    */
    void saveToDot(const Graph& graph, const std::string& dirPath, const std::string& fileName) const;

    /**
    *    Metoda zapisująca graf wczytany z pliku o rozszerzeniu dot do pliku o rozszerzeniu png.
    *    Rzuca wyjątek w razie niepowodzenia.
    */
    void saveDotAsPng(const std::string& dotDirPath, const std::string& pngDirPath, const std::string& fileName) const;

    /**
    *    Metoda zwracająca graf z pliku txt.
    *    Rzuca wyjątek w razie niepowodzenia.
    */
    Graph readFromTxt(const std::string& dirPath,const std::string& fileName) const;

};

class GraphFileIOException : public std::exception 
{
    const std::string info;

    public:
    GraphFileIOException(const std::string& msg);
    const char* what() const throw();
};

#endif