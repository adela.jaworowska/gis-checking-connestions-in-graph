import numpy as np
import matplotlib.pyplot as plt
import sys

fileName = sys.argv[1]
labelX = sys.argv[2]
if(len(sys.argv) == 4):
    title = sys.argv[3]
else:
    title = ''

x, y1, y2, y3, y4, y5, y6, y7 = np.loadtxt('../performanceTestsResults/' + fileName, unpack=True)

max_x = max(x)
max_y = max(max(y1), max(y2), max(y3), max(y4), max(y5), max(y6), max(y7))

fig = plt.figure(figsize=(24.0, 16.0))
ax = fig.add_subplot(1, 1, 1)

# And a corresponding grid
ax.grid(which='both')

# Or if you want different settings for the grids:
ax.grid(which='minor', alpha=0.1)
ax.grid(which='major', alpha=0.5)

# uncomment needed statistics:
ax.plot(x,y1, 'o-', label = 'średnia', markersize=4) 				# mean
ax.errorbar(x, y1, y2, label = 'odchylenie standardowe', fmt='o',ms=0, elinewidth= 0.5, capsize=3) # std
ax.plot(x,y3, 'go', label = 'min', markersize=2) 				# min 
ax.plot(x,y4, 'ro', label = 'max', markersize=2) 				# max
plt.plot(x,y5, label = 'kwartyl 1', linewidth=1, color='green', linestyle='--') 			# kwartyl 1
plt.plot(x,y6, label = 'mediana', linewidth=1, color='green', linestyle='--') 			# kwartyl 2
plt.plot(x,y7, label = 'kwartyl 3', linewidth=1, color='green', linestyle='--') 			# kwartyl 3

plt.xlabel(labelX, fontsize=25)
plt.ylabel('czas wykonania [ms]', fontsize=25)
plt.title(title, fontsize=25)

plt.xticks(rotation=90)
plt.legend(loc='best', fontsize=25)
plt.show()
