#include <iostream>
#include "graph.hpp"
#include "graphgenerator.hpp"
#include "graphfileio.hpp"
#include "interface.hpp"

int main()
{
    try {
        Interface interface;
        interface.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << '\n' << e.what() << '\n';
    }

    return 0;
}