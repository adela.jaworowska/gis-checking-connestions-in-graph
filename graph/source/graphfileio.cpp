#include "graphfileio.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

GraphFileIO::GraphFileIO() {}

GraphFileIO::~GraphFileIO() {}

void GraphFileIO::saveToTxt(const Graph& graph, const std::string& dirPath, const std::string& fileName) const
{
    const std::string path = dirPath + fileName;
    std::ofstream ofs(path);

    if(ofs.is_open())
    {
        for(const std::shared_ptr<const Vertex>& vertex : graph)
        {
            const int firstId = vertex->getId();
            ofs << firstId << " ";

            for(const std::weak_ptr<const Vertex>& neighbour : *vertex)
            {
                const int neighbourId = neighbour.lock()->getId();

                if(firstId<neighbourId)
                    ofs << neighbourId << " ";
            }
        
            ofs << std::endl;
        }

        ofs.close();
    }
    else
    {
        throw GraphFileIOException("Could not open and save to specified file!");
    }
}

void GraphFileIO::saveToDot(const Graph& graph, const std::string& dirPath, const std::string& fileName) const
{
    const std::string path = dirPath + fileName + ".dot";
    std::ofstream ofs(path);

    if(ofs.is_open())
    {
        ofs << "graph {" << std::endl;

        for(const std::shared_ptr<const Vertex>& vertex : graph)
        {
            const int &vertex1 = vertex->getId();

            if(vertex->begin() != vertex->end())
            {
                for(const std::weak_ptr<const Vertex>& neighbour : *vertex)
                {
                    const std::shared_ptr<const Vertex> sharedNeighbour = neighbour.lock();
                    if(vertex1 < sharedNeighbour->getId())
                    {
                        ofs << vertex->getId() << "--" << sharedNeighbour->getId() << std::endl;
                    }
                }
            }
            else
            {
                ofs << vertex->getId() << std::endl;
            }
        }

        ofs << "}";
        ofs.close();
    }
    else
    {
        throw GraphFileIOException("Could not open and save to specified file!");
    }
}

void GraphFileIO::saveDotAsPng(const std::string& dotDirPath, const std::string& pngDirPath, const std::string& fileName) const
{
    GVC_t *gvc = gvContext();
    
    FILE *inputFile = fopen((dotDirPath+fileName+".dot").c_str(), "r");

    if(!inputFile)
    {
        gvFreeContext(gvc);
        throw GraphFileIOException("Specified dot file does not exist!");
    }

    Agraph_t *g = agread(inputFile, 0);
    gvLayout(gvc, g, "dot");

    FILE *outputFile = fopen((pngDirPath+fileName+".png").c_str(), "w");

    if(!outputFile)
    {
        fclose(inputFile);
        gvFreeLayout(gvc, g);
        agclose(g);
        gvFreeContext(gvc);
        throw GraphFileIOException("Error occured when saving to png format!");
    }

    gvRender(gvc, g, "png", outputFile);

    gvFreeLayout(gvc, g);
    agclose(g);
    fclose(inputFile);
    fclose(outputFile);

    if(gvFreeContext(gvc) > 0)
        throw GraphFileIOException("Error occured during saving to png file!");
}

Graph GraphFileIO::readFromTxt(const std::string& dirPath, const std::string& fileName) const
{
    const std::string path = dirPath + fileName;
    std::ifstream ifs(path);

    if(ifs.is_open())
    {
        std::string line;
        std::vector<std::pair<int, std::vector<int>>> vertexIdAndEdgeEnds;
        Graph graph;
        while(std::getline(ifs, line))
        {
            std::istringstream iss(line);
            int id;
            iss>>id;
            graph.addVertex(id);
            const std::vector<int> neighbours((std::istream_iterator<int>(iss)), std::istream_iterator<int>());
            vertexIdAndEdgeEnds.emplace_back(id, std::move(neighbours));
        }

        for(const std::pair<int, std::vector<int>>& x : vertexIdAndEdgeEnds)
        {
            for(const int& id : x.second)
            {
                graph.addEdge(x.first, id);
            }
        }

        return graph;
    }

    throw GraphFileIOException("Could not open specified file!");
}

GraphFileIOException::GraphFileIOException(const std::string& msg) : info(msg) {}

const char* GraphFileIOException::what() const throw()
{
    return info.c_str();
}