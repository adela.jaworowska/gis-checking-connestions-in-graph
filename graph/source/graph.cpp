#include "graph.hpp"
#include <algorithm>
#include <stack>
#include <iostream>

void Graph::setAllToUnvisited() const
{
    for(const std::pair<int, std::shared_ptr<Vertex>>& vertex : vertices)
        vertex.second->visited=false;
}

Graph::Graph() {}

Graph::Graph(const Graph& other)
{
    std::vector<std::pair<int, int>> edges;
    for(const std::shared_ptr<const Vertex>& vertex : other)
    {
        const int firstId = vertex->getId();
        this->addVertex(firstId);
        for(const std::weak_ptr<const Vertex>& neighbour : *(vertex))
        {
            const int secondId = neighbour.lock()->getId();

            if(firstId<secondId)
                edges.emplace_back(firstId, secondId);
        }
    }

    for(const std::pair<int, int>& edge : edges)
    {
        this->addEdge(edge.first, edge.second);
    }
}

Graph::~Graph() {}
  
int Graph::addVertex(const int& id)
{
    bool insertSucceeded = vertices.insert(std::make_pair(id, std::make_shared<Vertex>(Vertex(id)))).second;
    return insertSucceeded ? id : -1;
}

bool Graph::addEdge(const int& vertex1, const int& vertex2)
{
    const auto itVertex1 = vertices.find(vertex1);

    if(itVertex1 != vertices.end())
    {
        const auto itVertex2 = vertices.find(vertex2);

        if(itVertex2 != vertices.end())
        {
            const std::shared_ptr<Vertex> vertex1Ptr = (*itVertex1).second;
            const std::shared_ptr<Vertex> vertex2Ptr = (*itVertex2).second;
            if( (vertex1Ptr)->addNeighbour(vertex2Ptr) != -1 &&  (vertex2Ptr)->addNeighbour(vertex1Ptr) != -1)
            {
                ++numberOfEdges;
                return true;
            }
        }
    }

    return false;
}

long long Graph::getNumberOfEdges() const
{
    return numberOfEdges;
}

const Vertex* Graph::getVertex(const int& id) const
{
    const auto it = vertices.find(id);
    return it != vertices.end() ? (*it).second.get() : nullptr;
}

Graph::VertexIterator Graph::begin() const
{
    return Graph::VertexIterator{vertices.begin()};
}

Graph::VertexIterator Graph::end() const
{
    return Graph::VertexIterator{vertices.end()};
}

int Graph::getNumberOfVertices() const
{ 
    return vertices.size();
}

Result Graph::areVerticesConnected(const int& id_vertex1, const int&  id_vertex2) const
{
    std::unordered_map<int, std::shared_ptr<Vertex>>::const_iterator it1, it2;

    if((it1 = vertices.find(id_vertex1)) != vertices.end() && (it2 = vertices.find(id_vertex2)) != vertices.end())
    {
        std::shared_ptr<Vertex> vertex1 = (*it1).second, vertex2 = (*it2).second;
        std::stack<std::shared_ptr<Vertex>> toBeVisited;

        vertex1->visited=true;
        toBeVisited.push(vertex1);

        while(!toBeVisited.empty())
        {
            std::shared_ptr<Vertex> currentVertex = toBeVisited.top();
            toBeVisited.pop();

            if(currentVertex->getId() == vertex2->getId())
            {
                setAllToUnvisited();
                if(vertex1->isNeighbour(currentVertex->getId()))
                {
                    return Result::Close;
                }
                else
                {
                    return Result::Transitive;
                }
            }

            for(std::weak_ptr<Vertex> neighbour : currentVertex->neighbours)
            {
                std::shared_ptr<Vertex> auxiliary = neighbour.lock();
                if(!auxiliary->visited)
                {
                    auxiliary->visited=true;
                    toBeVisited.push(auxiliary);
                }
            }
        }
    }

    setAllToUnvisited();
    return Result::None;
}