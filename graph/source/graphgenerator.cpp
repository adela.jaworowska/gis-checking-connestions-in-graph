#include "graphgenerator.hpp"
#include <random>
#include <algorithm>
#include <iostream>

void GraphGenerator::flushGeneratedEdges(Graph& graph, std::vector<std::pair<int,int>> edges, long long& currentNumberOfEdges, 
                             const long long& desiredNumberOfEdges) const
{
    unsigned long long i = 0;
    while(currentNumberOfEdges < desiredNumberOfEdges && i < edges.size())
    {
        const std::pair<int,int> edge = edges[i];
        if(graph.addEdge(edge.first, edge.second))
        {
            ++currentNumberOfEdges;
        }
        ++i;
    }
}

void GraphGenerator::addRandomConnectedGraph(Graph& graph, const int& numberOfVertices, const long long& numberOfEdges, const int& firstId) const
{
    const int lastId = numberOfVertices + firstId;
    std::vector<int> shuffledVerticesIds;
    shuffledVerticesIds.reserve(numberOfVertices);
    
    for(int i=firstId; i<lastId; ++i)
    {
        graph.addVertex(i);
        shuffledVerticesIds.push_back(i);
    }

    long long currentNumberOfEdges = 0;

    std::random_device randomDevice;
    std::mt19937 generator(randomDevice());

    std::shuffle(shuffledVerticesIds.begin(), shuffledVerticesIds.end(), generator);

    for(int i=0; i<numberOfVertices-1; ++i) // tworzenie losowej ścieżki w grafie
    {
        graph.addEdge(shuffledVerticesIds[i], shuffledVerticesIds[i+1]);
        ++currentNumberOfEdges;
    }
    
    if(currentNumberOfEdges < numberOfEdges)
    {
        const int vectorMaxSize = 100000000;
        std::vector<std::pair<int,int>> possibleEdges;
        possibleEdges.reserve(vectorMaxSize);
        int currentEdgeNumber = 0;
        
        for(const int i : shuffledVerticesIds) // tworzenie pozostałych krawędzi
        {
            for(int j=i+1; j<lastId; ++j)
            {
                possibleEdges.emplace_back(i, j);
                ++currentEdgeNumber;

                if(currentEdgeNumber == vectorMaxSize)  // przepełnienie wektora krawędzi
                {
                    std::shuffle(possibleEdges.begin(), possibleEdges.end(), generator);
                    flushGeneratedEdges(graph, possibleEdges, currentNumberOfEdges, numberOfEdges);
                    if(currentNumberOfEdges == numberOfEdges)
                        return;
                    currentEdgeNumber = 0;
                    possibleEdges.clear();
                }
            }
        }
        std::shuffle(possibleEdges.begin(), possibleEdges.end(), generator);
        flushGeneratedEdges(graph, possibleEdges, currentNumberOfEdges, numberOfEdges);
    }
}

GraphGenerator::GraphGenerator() {}

GraphGenerator::~GraphGenerator() {}

Graph GraphGenerator::generateGraph(const int& numberOfVertices, const int& numberOfComponents) const
{
    if(numberOfVertices < 1 || numberOfComponents < 1)
    {
        throw GraphGeneratorException("Wrong arguments! Requirements: numberOfVertices and numberOfComponents cannot be less than 1.");
    }
    
    if(numberOfVertices < numberOfComponents)
    {
        throw GraphGeneratorException("Wrong arguments! Requirements: numberOfVertices >= numberOfComponents.");
    }

    std::vector<int> graphLimesIds;
    graphLimesIds.reserve(numberOfComponents);

    std::random_device randomDevice;
    std::ranlux48 generator(randomDevice());

    if(numberOfComponents > 1)
    {
        std::uniform_int_distribution<int> distribution(1, numberOfVertices-1);
        int randomNumbers = 0;
        while(randomNumbers < numberOfComponents-1) // wygenerowanie "granic" poszczególnych grafów
        {
            const int generatedId = distribution(generator);
            if(std::find(std::begin(graphLimesIds), std::end(graphLimesIds), generatedId) == graphLimesIds.end())
            {
                graphLimesIds.push_back(generatedId);
                ++randomNumbers;
            }
        }
        graphLimesIds.push_back(numberOfVertices);
        std::sort(graphLimesIds.begin(), graphLimesIds.end());
    }
    else
    {
        graphLimesIds.push_back(numberOfVertices);
    }

    int firstId=1;
    Graph graph;

    for(const int id: graphLimesIds)
    {
        const int componentVertices = id-firstId+1;
        const long long maxPossibleEdges = (long long)componentVertices / 2 * (componentVertices-1);

        std::uniform_int_distribution<long long> distribution2(componentVertices-1, maxPossibleEdges);

        const long long componentEdges = distribution2(generator);
        addRandomConnectedGraph(graph, componentVertices, componentEdges, firstId);

        firstId = id+1;
    }

    return graph;
}

Graph GraphGenerator::generateConnectedGraph(const int& numberOfVertices, const long long& numberOfEdges) const
{
    const long long maxPossibleEdges = (long long)(numberOfVertices-1)/2*numberOfVertices;
    
    if(numberOfVertices < 1 || numberOfEdges < numberOfVertices-1 || numberOfEdges > maxPossibleEdges)
    {
        throw GraphGeneratorException("Wrong arguments! Such graph cannot be created");
    }

    Graph graph;
    addRandomConnectedGraph(graph, numberOfVertices, numberOfEdges, 1);
    return graph;
}

GraphGeneratorException::GraphGeneratorException(const std::string& msg) : info(msg) {}

const char* GraphGeneratorException::what() const throw()
{
    return info.c_str();
}