#include "vertex.hpp"
#include <algorithm>

Vertex::Vertex(const int& id) : id(id), visited(false) {}

Vertex::~Vertex() {}

const std::vector<std::weak_ptr<Vertex>>::const_iterator Vertex::findNeighbourById(const int& id) const
{
    long long begin = 0, end = neighbours.size()-1;

    while (begin<=end) 
    {
        const long long middle = (end+begin)/2;
        const int vertexId = neighbours[middle].lock()->getId();
        if(id > vertexId) 
        {
            begin = middle + 1;
        } 
        else if(id < vertexId)
        {
            end = middle - 1;
        }
        else
        {
            return neighbours.begin()+middle;
        }
    }
    return neighbours.end();
}


const bool Vertex::isNeighbour(const int& id) const
{
    return findNeighbourById(id) != neighbours.end();
}

int Vertex::addNeighbour(const std::shared_ptr<Vertex>& neighbour)
{
    auto it = std::lower_bound(neighbours.begin(), neighbours.end(), neighbour, IsSmaller());
    const bool isNeighbour = (it != neighbours.end()) && (neighbour->getId() >= (*it).lock()->getId());

    if(!isNeighbour && neighbour->getId() != (this->id)) 
    {
        neighbours.insert(it, neighbour);
        return neighbour->id;
    }

    return -1;
}

int Vertex::deleteNeighbour(const int& id)
{
    std::vector<std::weak_ptr<Vertex>>::const_iterator it = findNeighbourById(id);

    if(it != neighbours.end())
    {
        neighbours.erase(it);
        return id;
    }

    return -1;
}

const int& Vertex::getId() const
{
    return id;
}

Vertex::NeighbourIterator Vertex::begin() const
{
    return Vertex::NeighbourIterator{neighbours.begin()};
}

Vertex::NeighbourIterator Vertex::end() const
{
    return Vertex::NeighbourIterator{neighbours.end()};
}