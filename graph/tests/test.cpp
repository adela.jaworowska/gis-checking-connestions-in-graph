#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE RefactoringToolTests
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include "graphgenerator.hpp"
#include "graphfileio.hpp"


GraphGenerator generator;
Graph graph;
GraphFileIO fileIO;
boost::filesystem::path testGraphsPath = boost::filesystem::current_path() / "graph/tests/testgraphs/";

Graph generateGraph(const int& numberOfVertices, const int& numberOfSubsets) {
    return generator.generateGraph(numberOfVertices, numberOfSubsets); 
}

Graph generateConnectedGraph(const int& numberOfVertices, const int& numberOfSubsets) {
    return generator.generateConnectedGraph(numberOfVertices, numberOfSubsets); 
}

Graph getGraphFromFile(const std::string& fileName){
    return fileIO.readFromTxt(testGraphsPath.string(), fileName);
}

void saveGraphToFile(const Graph& graph, const std::string& fileName){
    fileIO.saveToTxt(graph, testGraphsPath.string(), fileName);
}

const int getNumberOfVertices(const Graph& graph){
    return graph.getNumberOfVertices();
}

std::vector<std::pair<int, int>> getSortedEdges(const Graph& graph){
    std::vector<std::pair<int, int>> edges;
    for(const std::shared_ptr<const Vertex>& vertex : graph)
    {
        const int firstId = vertex->getId();
        for(const std::weak_ptr<const Vertex>& neighbour : *(vertex))
        {
            const int secondId = neighbour.lock()->getId();

            if(firstId<secondId)
                edges.emplace_back(firstId, secondId);
        }
    }
    std::sort(edges.begin(), edges.end());

    return edges;
}

bool isEdgeInGraph(const Graph& graph, std::pair<int, int> edge){
   const Vertex* vertex = graph.getVertex(edge.first);

   if(vertex != nullptr)
   {
       for(const auto& neighbour : *vertex)
       {
           const std::shared_ptr<const Vertex> neighbourShared = neighbour.lock();
            if(neighbourShared->getId() == edge.second && neighbourShared->isNeighbour(edge.first))
                return true; 
       }
   }
    return false;
}

BOOST_AUTO_TEST_SUITE(PositiveTests)

BOOST_AUTO_TEST_CASE( checkNumberOfVertices_graphFromGenerator_test )
{                         
    BOOST_CHECK_EQUAL(getNumberOfVertices(generateGraph(10, 3)), 10);
}

BOOST_AUTO_TEST_CASE( checkNumberOfVertices_graphFromFile_test )
{                         
    BOOST_CHECK_EQUAL( getNumberOfVertices(getGraphFromFile("graph_5")), 5);
}

BOOST_AUTO_TEST_CASE( eachVertexExists_graphFromGeneretor_test )
{                         
    Graph graph = generateGraph(5,2);

    BOOST_CHECK(graph.getVertex(1) != nullptr );
    BOOST_CHECK(graph.getVertex(2) != nullptr );
    BOOST_CHECK(graph.getVertex(3) != nullptr );
    BOOST_CHECK(graph.getVertex(4) != nullptr );
    BOOST_CHECK(graph.getVertex(5) != nullptr );
}

BOOST_AUTO_TEST_CASE( eachVertexExists_graphFromFile_test )
{                         
    Graph graph = getGraphFromFile("graph_5");  

    BOOST_CHECK(graph.getVertex(1) != nullptr );
    BOOST_CHECK(graph.getVertex(2) != nullptr );
    BOOST_CHECK(graph.getVertex(3) != nullptr );
    BOOST_CHECK(graph.getVertex(4) != nullptr );
    BOOST_CHECK(graph.getVertex(5) != nullptr );
}

BOOST_AUTO_TEST_CASE( checkNumberOfEdges_graphFromFile_test )
{                         
    Graph graph = getGraphFromFile("graph_5");  

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 9);
}

BOOST_AUTO_TEST_CASE( eachEdgeExists_graphFromFile_test )
{               
    Graph graph = getGraphFromFile("graph_5");          

    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,2)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,3)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,5)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,1)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,3)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,4)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,5)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,1)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,2)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,4)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,5)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(4,2)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(4,3)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,1)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,2)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,3)) == true);
}

BOOST_AUTO_TEST_CASE( saveGraphToFile_test )
{             
    Graph graph;
    graph.addVertex(1);
    graph.addVertex(2);
    graph.addVertex(3);
    graph.addVertex(4);
    graph.addVertex(5);

    graph.addEdge(1,2);
    graph.addEdge(2,3);
    graph.addEdge(3,4);
    graph.addEdge(4,5);
    graph.addEdge(1,4);
    graph.addEdge(2,5);

    saveGraphToFile(graph, "tempFile");
    graph = getGraphFromFile("tempFile");

    BOOST_CHECK_EQUAL( getNumberOfVertices(graph), 5);  

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 6);

    BOOST_CHECK(graph.getVertex(1) != nullptr );
    BOOST_CHECK(graph.getVertex(2) != nullptr );
    BOOST_CHECK(graph.getVertex(3) != nullptr );
    BOOST_CHECK(graph.getVertex(4) != nullptr );
    BOOST_CHECK(graph.getVertex(5) != nullptr );

    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,2)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,3)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,4)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(4,5)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,4)) == true);
    BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,5)) == true);
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnGraphWithNoEdges_test )
{             
    Graph graph = generateGraph(5,5);

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 0);

    for(int i = 1; i<graph.getNumberOfVertices()-1; ++i)
    {
        for(int j = i+1; j<graph.getNumberOfVertices()-1; ++j)
        {
            BOOST_CHECK(graph.areVerticesConnected(i,j) == Result::None);
        }
    }
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnCoherentGraph_test )
{             
    Graph graph = generateGraph(5,1);

    for(int i = 1; i<graph.getNumberOfVertices()-1; ++i)
    {
        for(int j = i+1; j<graph.getNumberOfVertices()-1; ++j)
        {
            const Result result = graph.areVerticesConnected(i,j);
            BOOST_CHECK(result != Result::None);
        }
    }
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnFullGraph_test )
{             
    Graph graph = getGraphFromFile("fullGraphForTests");

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 10);

    for(int i = 1; i<graph.getNumberOfVertices()-1; ++i)
    {
        for(int j = i+1; j<graph.getNumberOfVertices()-1; ++j)
        {
            const Result result = graph.areVerticesConnected(i,j);
            BOOST_CHECK(result == Result::Close);
        }
    }
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnTreeGraph_test )
{             
    Graph graph = getGraphFromFile("treeGraphForTests");

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 6);

    int numOfVertices = graph.getNumberOfVertices();

    for(int i = 1; i<numOfVertices -1; ++i)
    {
        for(int j = i+1; j<numOfVertices -1; ++j)  
        {
            if(j == 2*i || j == 2*i+1){
                BOOST_CHECK(graph.areVerticesConnected(i,j) == Result::Close);
            }
            else{
                BOOST_CHECK(graph.areVerticesConnected(i,j) == Result::Transitive);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnCircleGraph_test )
{             
    Graph graph = getGraphFromFile("circleGraphForTests");

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 5);

    int numOfVertices = graph.getNumberOfVertices();

    for(int i = 1; i<numOfVertices; ++i)
    {
        const Result closeResult = graph.areVerticesConnected(i, i+1);
        BOOST_CHECK(closeResult == Result::Close);

        for(int j = 0; j<numOfVertices-3; ++j)
        {
            Result transResult;
            if(i+2+j == 5){
                transResult = graph.areVerticesConnected(i, 5);
            }
            else{
                transResult = graph.areVerticesConnected(i, (i+2+j) % 5);
            }
            BOOST_CHECK(transResult == Result::Transitive);
        }
    }
}

BOOST_AUTO_TEST_CASE( performAlgorithmOnThreeComponentsGraph_test )
{             
    Graph graph = getGraphFromFile("threeComponentsGraphForTests");

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 9);

    BOOST_CHECK(graph.areVerticesConnected(1,2) == Result::Close);
    BOOST_CHECK(graph.areVerticesConnected(2,3) == Result::Close);
    BOOST_CHECK(graph.areVerticesConnected(4,5) == Result::Close);
    BOOST_CHECK(graph.areVerticesConnected(4,7) == Result::Transitive);
    BOOST_CHECK(graph.areVerticesConnected(6,8) == Result::None);
    BOOST_CHECK(graph.areVerticesConnected(3,9) == Result::None);
}


BOOST_AUTO_TEST_CASE( generateConnectedFullGraph_test )
{             
    Graph graph = generateConnectedGraph(5, 10);

    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 10);
    BOOST_CHECK_EQUAL(graph.getNumberOfVertices(), 5);


    for(int i = 1; i<graph.getNumberOfVertices()-1; ++i)
    {
        BOOST_CHECK(graph.getVertex(i) != nullptr );

        for(int j = i+1; j<graph.getNumberOfVertices()-1; ++j)
        {
            BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(i,j)) == true);
        }
    }

    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,2)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,3)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(1,5)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,1)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,3)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,4)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(2,5)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,1)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,2)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,4)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(3,5)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(4,2)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(4,3)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,1)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,2)) == true);
    // BOOST_CHECK(isEdgeInGraph(graph, std::make_pair(5,3)) == true);   
}

BOOST_AUTO_TEST_CASE( generateConnectedRareGraph_test )
{             
    Graph graph = generateConnectedGraph(100, 100);

    BOOST_CHECK_EQUAL(graph.getNumberOfVertices(), 100);
    BOOST_CHECK_EQUAL(graph.getNumberOfEdges(), 100);

    for(int i = 1; i<graph.getNumberOfVertices()-1; ++i)
    {
        BOOST_CHECK(graph.getVertex(i) != nullptr );
    }
}

BOOST_AUTO_TEST_CASE(throwGraphGeneratorException_generateConnectedGraph_test )
{             
    BOOST_CHECK_THROW(generateConnectedGraph(10, 100), GraphGeneratorException);
}

BOOST_AUTO_TEST_CASE(throwGraphGeneratorException_generateGraph_test )
{             
    BOOST_CHECK_THROW(generateGraph(10, 100), GraphGeneratorException);
}

BOOST_AUTO_TEST_SUITE_END()

// EOF