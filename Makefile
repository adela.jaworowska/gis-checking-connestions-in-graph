CC = g++
GRAPHVIZFLAGS = -lgvc -lcgraph -lcdt
CPPFLAGS = -Wall -pedantic -std=c++11 -lboost_system -lboost_filesystem $(GRAPHVIZFLAGS)
CPPTESTFLAGS = $(CPPFLAGS) -lboost_unit_test_framework
OPTS = -O2

all: testExec graph
	./testexec --log_level=test_suite
	rm graph/tests/testgraphs/tempFile
	
testExec:
	$(CC) $(OPTS) -o testexec -Iheaders graph/source/vertex.cpp graph/source/graph.cpp graph/source/graphgenerator.cpp graph/source/graphfileio.cpp graph/tests/test.cpp $(CPPTESTFLAGS)

graph:
	$(CC) $(OPTS) -o graphexec -Iheaders graph/source/vertex.cpp graph/source/graph.cpp graph/source/graphgenerator.cpp graph/source/graphfileio.cpp interface/interface.cpp performancemeasurer/performancemeasurer.cpp performancemeasurer/statistics.cpp main.cpp $(CPPFLAGS) $(GRAPHVIZFLAGS)

clean:
	rm testexec graphexec
