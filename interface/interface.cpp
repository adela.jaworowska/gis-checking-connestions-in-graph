#include "interface.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <exception>

void Interface::createDirIfNonexistent(const boost::filesystem::path& path)
{
    if(!boost::filesystem::exists(path) || !boost::filesystem::is_directory(path))
    {
        boost::filesystem::create_directory(path);
    }
}

void Interface::printHelp()
{
    std::cout << font.at("SKYF") << std::endl;
    std::cout << "generate <numberOfVertices> <numberOfComponents>" << font.at("BGREENF") << " - generate a new graph and load it into the program" << font.at("SKYF") << std::endl;
    std::cout << "generateConnected <numberOfVertices> <numberOfEdges>" << font.at("BGREENF") << " - generate a new connected graph and load it into the program" << font.at("SKYF") << std::endl;
    std::cout << "load <fileName>" << font.at("BGREENF") << " - load a graph from the specified txt file" << font.at("SKYF") << std::endl;
    std::cout << "save <graphName>" << font.at("BGREENF") <<  " - save a graph loaded into the program in txt and dot formats" << font.at("SKYF") << std::endl;
    std::cout << "dotToPng <fileName>" << font.at("BGREENF") << " - create a png image of the graph saved to the specified dot file" << font.at("SKYF") << std::endl;
    std::cout << "checkNeighbourhood <vertex1Id> <vertex2Id>" << font.at("BGREENF") << " - check neighbourhood relation between vertex 1 and vertex 2" << font.at("SKYF") << std::endl;
    std::cout << "lsTxt" << font.at("BGREENF") << "- print names of graphs saved in txt format " << font.at("SKYF") << std::endl;
    std::cout << "lsDot" << font.at("BGREENF") << " - print names of graphs saved in dot format " << font.at("SKYF") << std::endl;
    std::cout << "lsPng" << font.at("BGREENF") << " - print names of graphs saved in png format " << font.at("SKYF") << std::endl;
    std::cout << "printGraph" << font.at("BGREENF") << " - prints currently loaded graph " << font.at("SKYF") << std::endl;
    std::cout << "performVertexMeasurements <outputFileName> <numberOfVertices> <numberOfExaminedGraphs> <numberOfQueriesPerGraph>" 
    << font.at("BGREENF") << " - measure algorithm's performance for graphs of same size" << font.at("SKYF") << std::endl;
    std::cout << "performVertexMeasurements <outputFileName> <numberOfVertices (smallest examined graph)> <vertexIncrementationValue> <numberOfGraphSizes> <numberOfExaminedGraphs> <numberOfQueriesPerGraph>" 
    << font.at("BGREENF") << " - measure algorithm's performance for graphs of different (increasing) sizes" << font.at("SKYF") << std::endl;
    std::cout << "performEdgeMeasurements <outputFileName> <numberOfVertices> <numberOfEdges> <numberOfQueries>" 
    << font.at("BGREENF") << "  - measure algorithm's performance for specified graph" << font.at("SKYF") << std::endl;
    std::cout << "performEdgeMeasurements <outputFileName> <numberOfVertices> <minimalNumberOfEdges> <edgeIncrementationValue> <numberOfExaminedGraphs> <numberOfQueriesPerGraph>" 
    << font.at("BGREENF") << "  - measure algorithm's performance for graphs that have the same number of vertices; acquire results for different numbers of edges" << font.at("SKYF") << std::endl;
    std::cout << "quit" << font.at("BGREENF") << " - quit the program" << font.at("RESETF") << std::endl << std::endl;
}

void Interface::printGraph(const Graph& graph)
{
    std::cout << font.at("BGREENF") << std::endl;

    for(std::shared_ptr<const Vertex> vertex : graph)
    {
        std::cout << vertex->getId() << " -> ";

        for(const std::weak_ptr<const Vertex>& neighbour : *vertex)
        {
            std::cout << neighbour.lock()->getId() << " ";
        }
        std::cout << std::endl;
    }

    std::cout << font.at("RESETF") << std::endl;
}

void Interface::handleGenerate(const std::vector<std::string>& args)
{
    if(args.size() != 3)
    {
        throw InterfaceException("Wrong number of arguments!");
    }
    graph = Graph(); // clear previous graph
    graph = generator.generateGraph(std::stoi(args[1]), std::stoll(args[2]));
}

void Interface::handleGenerateConnected(const std::vector<std::string>& args)
{
    if(args.size() != 3)
    {
        throw InterfaceException("Wrong number of arguments!");
    }
    graph = Graph(); // clear previous graph
    graph = generator.generateConnectedGraph(std::stoi(args[1]), std::stoll(args[2]));
}

void Interface::handleLoadFromFile(const std::vector<std::string>& args)
{
    if(args.size() != 2)
    {
        throw InterfaceException("Wrong number of arguments!");
    }
    graph = Graph(); // clear previous graph
    graph = fileIO.readFromTxt(txtFilesPath.string(), args[1]);
}

void Interface::handleSaveToFile(const std::vector<std::string>& args)
{
    if(args.size() != 2)
    {
        throw InterfaceException("Wrong number of arguments!");
    }

    fileIO.saveToTxt(graph, txtFilesPath.string(), args[1]);
    fileIO.saveToDot(graph, dotFilesPath.string(), args[1]);
}

void Interface::handleSaveToPng(const std::vector<std::string>& args)
{
    if(args.size() != 2)
    {
        throw InterfaceException("Wrong number of arguments!");
    }

    fileIO.saveDotAsPng(dotFilesPath.string(), pngFilesPath.string(), args[1]);
}

void Interface::handlePrintDirContents(const boost::filesystem::path& path)
{
    if(boost::filesystem::exists(path) && boost::filesystem::is_directory(path))
    {
        std::cout << font.at("BGREENF") << std::endl;
        for(boost::filesystem::directory_entry& entry : boost::filesystem::directory_iterator(path))
        {
            std::cout << entry.path().stem() << std::endl;
        }
    }
    else
    {
        throw InterfaceException(path.string() + " - specified path does not exist!");
    }

    std::cout << font.at("RESETF") << std::endl;
}

void Interface::handlePerformAlgorithm(const std::vector<std::string>& args)
{
    if(args.size() != 3)
    {
        throw InterfaceException("Wrong number of arguments!");
    }
    
    std::cout << font.at("BGREENF") << std::endl
              << "Connection between given vertices is: " 
              << toString( graph.areVerticesConnected(std::stoi(args[1]), std::stoi(args[2])) ) 
              << font.at("RESETF") << std::endl << std::endl;
}

void Interface::handlePerformVertexMeasurements(const std::vector<std::string>& args)
{
    if(args.size() == 5)
    {
        performanceMeasurer.measureVertexPerformance(performanceTestsFilesPath.string(), args[1], std::stoi(args[2]), 
                                                0, 1, std::stoi(args[3]), std::stoi(args[4]));
    }
    else if(args.size() == 7)
    {
        performanceMeasurer.measureVertexPerformance(performanceTestsFilesPath.string(), args[1], std::stoi(args[2]), 
                                                std::stoi(args[3]), std::stoi(args[4]), std::stoi(args[5]), std::stoi(args[6]));
    }
    else
    {
        throw InterfaceException("Wrong number of arguments!");
    }
}

void Interface::handlePerformEdgeMeasurements(const std::vector<std::string>& args)
{
    if(args.size() == 5)
    {
        performanceMeasurer.measureEdgePerformance(performanceTestsFilesPath.string(), args[1], std::stoi(args[2]), 
                                                   std::stoll(args[3]), 0, 1, std::stoi(args[4]));
    }
    else if(args.size() == 7)
    {
        performanceMeasurer.measureEdgePerformance(performanceTestsFilesPath.string(), args[1], std::stoi(args[2]), 
                                                std::stoll(args[3]), std::stoll(args[4]), std::stoi(args[5]), std::stoi(args[6]));
    }
    else
    {
        throw InterfaceException("Wrong number of arguments!");
    }
}

void Interface::getUserCommands()
{
    if (FD_ISSET(0, &ready))
    {
        char buffer[4096];

        if(fgets(buffer, 4096, stdin) == NULL)
        {
            throw InterfaceException("I/O error");
        }

        processCommands(buffer);
    }
}

void Interface::processCommands(const char *buf)
{
    buffer.insert(buffer.end(), buf, buf + strlen(buf));
    for (auto it = buffer.begin(); it != buffer.end();)
    {
        if (*it == 10)
        {
            commandQueue.emplace(std::string(buffer.begin(), it));
            it = buffer.erase(buffer.begin(), it + 1);
        }
        else
        {
            ++it;
        }
    }
}

std::vector<std::string> Interface::splitBySpace(const std::string &input)
{
    std::string buf;
    std::stringstream ss(input);

    std::vector<std::string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    return tokens;
}

void Interface::handleCommands()
{
    if (!commandQueue.empty())
    {
        std::string input = commandQueue.front();
        commandQueue.pop();
        std::vector<std::string> tokens = splitBySpace(input);
        if (tokens.size() == 0)
            tokens.push_back("");

        const std::string firstArg = tokens[0];

        if (firstArg == "help")
        {
            printHelp();
        }
        else if (firstArg == "generate")
        {
            handleGenerate(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "generateConnected")
        {
            handleGenerateConnected(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "load")
        {
            handleLoadFromFile(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "save")
        {
            handleSaveToFile(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "dotToPng")
        {
            handleSaveToPng(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "checkNeighbourhood")
        {
            handlePerformAlgorithm(tokens);
        }
        else if (firstArg == "lsTxt")
        {
            handlePrintDirContents(txtFilesPath);
        }
        else if (firstArg == "lsDot")
        {
            handlePrintDirContents(dotFilesPath);
        }
        else if (firstArg == "lsPng")
        {
            handlePrintDirContents(pngFilesPath);
        }
        else if (firstArg == "performVertexMeasurements")
        {
            handlePerformVertexMeasurements(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "performEdgeMeasurements")
        {
            handlePerformEdgeMeasurements(tokens);
            std::cout << font.at("BGREENF") << "Done" << font.at("RESETF") << std::endl << std::endl;
        }
        else if (firstArg == "printGraph")
        {
            printGraph(graph);
        }
        else if (firstArg == "quit")
        {
            breakFlag=true;
        }
        else
        {
            std::cout << font.at("REDF") << "Incorrect command! Type 'help' in order to see the list of available commands!." << font.at("RESETF") << std::endl;
        }

    }
}

Interface::Interface() 
{
    generatedGraphsPath = boost::filesystem::current_path() / "graph/generatedGraphs";
    dotFilesPath = generatedGraphsPath/"dot/";
    txtFilesPath = generatedGraphsPath/"txt/";
    pngFilesPath = generatedGraphsPath/"png/";
    performanceTestsFilesPath = boost::filesystem::current_path() / "performanceTestsResults/";

    createDirIfNonexistent(generatedGraphsPath);
    createDirIfNonexistent(dotFilesPath);
    createDirIfNonexistent(txtFilesPath);
    createDirIfNonexistent(pngFilesPath);
    createDirIfNonexistent(performanceTestsFilesPath);
}

Interface::~Interface() {}


void Interface::run()
{
    std::cout << "Program is running. Type 'help' in order to see the list of available commands!." << std::endl << std::endl;

    while (!breakFlag)
    {
        FD_ZERO(&ready);
        FD_SET(0, &ready);

        to.tv_sec = 1;
        to.tv_usec = 0;

        if ((select(1, &ready, (fd_set *)0, (fd_set *)0, &to)) == -1)
        {
            throw InterfaceException("I/O error - select");
        }

        try {
            getUserCommands();
            handleCommands();
        }
        catch(const std::invalid_argument& e)
        {
            std::cerr << font.at("REDF") << '\n' << "Expected integer argument!" << font.at("RESETF") << '\n';
        }
        catch(const std::bad_alloc& e)
        {
            std::cerr << font.at("REDF") << '\n' << "Memory limit exceeded!" << font.at("RESETF") << '\n';
            break;
        }
        catch(const std::exception& e)
        {
            std::cerr << font.at("REDF") << '\n' << e.what() << font.at("RESETF") << '\n';
        }
    }
}

InterfaceException::InterfaceException(const std::string& msg) : info(msg) {}

const char* InterfaceException::what() const throw()
{
    return info.c_str();
}