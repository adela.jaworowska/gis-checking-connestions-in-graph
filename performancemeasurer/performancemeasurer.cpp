#include "performancemeasurer.hpp"

std::chrono::steady_clock::time_point PerformanceMeasurer::now() const
{
    return std::chrono::steady_clock::now();
}

double PerformanceMeasurer::elapsedTime(const TimePoint& begin, const TimePoint& end) const
{
    return std::chrono::duration<double, std::milli>(end-begin).count();
}

std::pair<int, int> PerformanceMeasurer::generateCheckedVertices(const int& n) const
{
    std::random_device randomDevice;
    std::mt19937 generator(randomDevice());
    std::uniform_int_distribution<int> distribution(1, n);
    const int first = distribution(generator);
    int second = first;

    while(second==first)
        second = distribution(generator);

    return std::make_pair(first, second);
}

void PerformanceMeasurer::measureQueriesPerGraph(const Graph& graph, PerformanceMeasurer::Statistics & statistics, const int& queriesPerGraph) const
{
    for(int j=0;j<queriesPerGraph;++j)
    {
        std::pair<int,int> checkedVertices = generateCheckedVertices(graph.getNumberOfVertices());

        const TimePoint begin = now();
        graph.areVerticesConnected(checkedVertices.first, checkedVertices.second);
        const TimePoint end = now();

        statistics.loadValue(elapsedTime(begin, end));
    }
}

PerformanceMeasurer::Statistics PerformanceMeasurer::measurePerformanceForN(const int& n, const int& trialGraphs, const int& queriesPerGraph) const
{
    Statistics statistics;
    for(int i=0;i<trialGraphs;++i)
    {
        Graph graph = generator.generateGraph(n, 1);
        measureQueriesPerGraph(graph, statistics, queriesPerGraph);
    }

    statistics.calculateStdDev();
    statistics.calculateQuantiles();

    return statistics;
}

PerformanceMeasurer::PerformanceMeasurer() {}

PerformanceMeasurer::~PerformanceMeasurer() {}

void PerformanceMeasurer::measureVertexPerformance(const std::string& resultDirPath, const std::string& fileName, const int& firstN,
                            const int& deltaN, const int& numberOfMeasurements, const int& trialGraphs, const int& queriesPerGraph) const
{
    if(firstN < 1 || deltaN < 0 || trialGraphs < 1 || queriesPerGraph < 1)
    {
        throw PerformanceException("Improper argument values!");
    }

    const std::string path = resultDirPath + fileName;
    std::ofstream ofs(path);

    if(!ofs.is_open())
    {
        throw PerformanceException("Could not open and save to specified file! : " + path);
    }

    int currentN = firstN;
    
    for(int i=0;i<numberOfMeasurements;++i)
    {
        ofs << currentN << " " << measurePerformanceForN(currentN, trialGraphs, queriesPerGraph) << std::endl;
        currentN += deltaN;
    }

    ofs.close();
}

void PerformanceMeasurer::measureEdgePerformance(const std::string& resultDirPath, const std::string& fileName, const int& n,
                                                 const long long& minEdges, const long long& deltaEdges, const int& trialGraphs, 
                                                 const int& queriesPerGraph) const
{
    const long long maxPossibleEdges = (long long) (n-1)/2*n;
    const long long desiredEdges = (long long) minEdges+(deltaEdges*trialGraphs);
    if(n < 1 || trialGraphs < 1 || queriesPerGraph < 1 || minEdges < n-1 || desiredEdges > maxPossibleEdges)
    {
        throw PerformanceException("Improper argument values!");
    }

    const std::string path = resultDirPath + fileName;
    std::ofstream ofs(path);

    if(!ofs.is_open())
    {
        throw PerformanceException("Could not open and save to specified file! : " + path);
    }

    long long numberOfEdges = minEdges;

    for(int i=0; i<trialGraphs; ++i)
    {
        Graph graph = generator.generateConnectedGraph(n, numberOfEdges);
        Statistics statistics;
        measureQueriesPerGraph(graph, statistics, queriesPerGraph);

        statistics.calculateStdDev();
        statistics.calculateQuantiles();
        ofs << numberOfEdges << " " << statistics << std::endl;

        numberOfEdges += deltaEdges;
    }
}

PerformanceException::PerformanceException(const std::string& msg) : info(msg) {}

const char* PerformanceException::what() const throw()
{
    return info.c_str();
}