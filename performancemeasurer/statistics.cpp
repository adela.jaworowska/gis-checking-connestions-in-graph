#include "performancemeasurer.hpp"

void PerformanceMeasurer::Statistics::calculateQuantilesEven()
{
    const int middleIndex = values.size()/2 - 1;
    quartile2 = (values[middleIndex] + values[middleIndex+1]) / 2;

    if(values.size() == 2)
    {
        quartile1=values[0];
        quartile3=values[1];
        return;
    }

    const int quarterIndex = middleIndex/2;
    quartile1 = values[quarterIndex];
    const int quarter3Index = quarterIndex+middleIndex+1;
    quartile3 = values[quarter3Index];
}

void PerformanceMeasurer::Statistics::calculateQuantilesOdd()
{
    quartile2 = values[values.size()/2];

    if(values.size() == 1)
    {
        quartile1=quartile3=values[0];
        return;
    }
    
    const int quarterIndex = values.size()/4;
    const int quarter3Index = 3*quarterIndex;

    if(values.size() % 4 == 1)
    {
        quartile1 = 0.25*values[quarterIndex] + 0.75*values[quarterIndex+1];
        quartile3 = 0.75*values[quarter3Index+1] + 0.25*values[quarter3Index+2];
    }
    else
    {
        quartile1 = 0.75*values[quarterIndex+1] + 0.25*values[quarterIndex+2];
        quartile3 = 0.25*values[quarter3Index+2] + 0.75*values[quarter3Index+3];
    }

}

void PerformanceMeasurer::Statistics::loadValue(const double& value)
{
    values.push_back(value);
    min = value<min ? value : min;
    max = value>max ? value : max;
    mean+=(value-mean)/values.size();
}

void PerformanceMeasurer::Statistics::calculateStdDev()
{
    double accumulator = 0;
    std::for_each(values.begin(), values.end(),[&](const double& x) {accumulator += (x-mean)*(x-mean);});
    standardDeviation = values.size() > 1 ? std::sqrt(accumulator/(values.size()-1)) : 0;
}

void PerformanceMeasurer::Statistics::calculateQuantiles()
{
    if(values.size() != 0)
    {
        std::sort(values.begin(), values.end());
    }
    else
    {
        quartile1=quartile2=quartile3=0;
        return;
    }
    
    if(values.size() % 2 == 0)
    {
        calculateQuantilesEven();
    }
    else
    {
        calculateQuantilesOdd();
    } 
}