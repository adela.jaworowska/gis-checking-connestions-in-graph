#!/bin/sh

declare -a libs=('libboost-all-dev' 'graphviz' 'libgraphviz-dev' 'graphviz-dev')

apt-get update

for i in "${libs[@]}"
do
    apt-get install $i
done